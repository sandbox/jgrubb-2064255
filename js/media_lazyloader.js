(function($) {
  Drupal.behaviors.mediaLazyloader = {
    attach: function() {
      $('img.lazy').show().lazyload();
    }
  }
}(jQuery));
